<!DOCTYPE html>

<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/table.css"/>">
        <title>Trainee List</title>
    </head>

    <body>

        <div class="element">

            <div class="sidebar">
                <img src="<c:url value="/public/storage/therap.png"/>">

                <form method="get" action="<c:url value="/allcourse"/>">
                    <button type="submit">All Course</button>
                </form>

                <form method="get" action="<c:url value="/addcourse"/>">
                    <button type="submit">Add Course</button>
                </form>

                <form method="get" action="<c:url value="/deletecourse"/>">
                    <button type="submit">Delete Course</button>
                </form>

                <form method="get" action="<c:url value="/traineelist"/>">
                    <button type="submit">Trainee List</button>
                </form>

                <form method="get" action="<c:url value="/register"/>">
                    <button type="submit">Trainee Registration</button>
                </form>

                <form method="get" action="<c:url value="/logout"/>">
                    <button type="submit">Log Out</button>
                </form>
            </div>

            <div align="center">
                <table class="content-table">
                    <thead>
                    <tr>
                        <th>Trainee Name</th>
                        <th>Trainee Email</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${traineelist}" var="trainee">
                        <tr>
                            <td>
                                <c:out value="${trainee.name}"/>
                            </td>
                            <td>
                                <c:out value="${trainee.email}"/>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>

    </body>
</html>

