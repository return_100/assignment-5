<!DOCTYPE html>

<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
        <title>Add Course</title>
    </head>

    <body>

        <div class="element">

            <div class="sidebar">
                <img src="<c:url value="/public/storage/therap.png"/>">

                <form method="get" action="<c:url value="/allcourse"/>">
                    <button type="submit">All Course</button>
                </form>

                <form method="get" action="<c:url value="/addcourse"/>">
                    <button type="submit">Add Course</button>
                </form>

                <form method="get" action="<c:url value="/deletecourse"/>">
                    <button type="submit">Delete Course</button>
                </form>

                <form method="get" action="<c:url value="/traineelist"/>">
                    <button type="submit">Trainee List</button>
                </form>

                <form method="get" action="<c:url value="/register"/>">
                    <button type="submit">Trainee Registration</button>
                </form>

                <form method="get" action="<c:url value="/logout"/>">
                    <button type="submit">Log Out</button>
                </form>
            </div>

            <div class="input-grid">
                <c:out value="${message}"/>

                <form action="<c:url value="/addcourse"/>" method="post">
                    <input type="text" placeholder="Course Name" name="courseName" value="${param.courseName}" required>
                    <br>

                    <button class="add-button" type="submit">ADD</button>
                </form>
            </div>

        </div>

    </body>
</html>