<!DOCTYPE html>

<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/design.css"/>">
        <title>Login</title>
    </head>

    <body>

        <div class="root">
            <div>
                <img src="<c:url value="/public/storage/therap.png"/>">
            </div>

            <div>
                <form method="POST" action="<c:url value="/login"/>">
                    <div>
                        <input id="Admin" type="radio" name="role" value="Admin" required>
                        <label for="Admin">ADMIN</label>
                        <input id="Trainee" type="radio" name="role" value="Trainee" required>
                        <label for="Trainee">TRAINEE</label>
                    </div>

                    <div class="input-container">
                        <input class="text-field-input" type="text" placeholder="Email" name="email" value="${param.email}" required>
                        <input class="text-field-input" type="password" placeholder="Password" name="password" required>
                    </div>

                    <button type="submit">Login</button>
                </form>
            </div>
        </div>

    </body>
</html>