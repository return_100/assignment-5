<!DOCTYPE html>

<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/table.css"/>">
        <title>Enrolled Course</title>
    </head>

    <body>

        <div class="element">

            <div class="sidebar">
                <img src="<c:url value="/public/storage/therap.png"/>">

                <form action="<c:url value="/enrolledcourse"/>" method="get">
                    <button type="submit">Enrolled Course</button>
                </form>

                <form action="<c:url value="/traineeallcourse"/>" method="get">
                    <button type="submit">All Course</button>
                </form>

                <form action="<c:url value="/traineeaddcourse"/>" method="get">
                    <button type="submit">Add Course</button>
                </form>

                <form action="<c:url value="/traineeremovecourse"/>" method="get">
                    <button type="submit">Remove Course</button>
                </form>

                <form action="<c:url value="/logout"/>" method="get">
                    <button type="submit">Log Out</button>
                </form>
            </div>

            <div>
                <table class="content-table">
                    <thead>
                    <tr>
                        <th>Enrolled Course Name</th>
                    </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${enrolledcourse}" var="course">
                            <tr>
                                <td>
                                    <c:out value="${course.name}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>

    </body>
</html>

