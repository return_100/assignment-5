<!DOCTYPE HTML>

<%@ page isErrorPage="true" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/design.css"/>">
        <title>Exception</title>
    </head>

    <body class="error">
        <%
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

            response.getWriter().println("Exception Occured!!<br>");
            response.getWriter().println("Excaption Type: ");
            response.getWriter().println(exception.toString() + "<br>");
            response.getWriter().println("Please go back");
        %>
    </body>

</html>