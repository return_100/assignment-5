<!DOCTYPE HTML>

<%@ page isErrorPage="true" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/design.css"/>">
        <title>Error</title>
    </head>

    <body class="error">
        <%
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

            if (response.getStatus() == 404) {
                response.getWriter().println(404 + " - Not Found<br>");
            } else if (response.getStatus() == 500) {
                response.getWriter().println(500 + " - Internal Server Error<br>");
            } else if (response.getStatus() == 503) {
                response.getWriter().println(503 + " - Service Unavailable<br>");
            } else if (response.getStatus() == 405) {
                response.getWriter().println(405 + " - Method Not Allowed<br>");
            } else {
                response.getWriter().println(response.getStatus() + " - Unknown Error<br>");
            }

            response.getWriter().println("Please Go Back");
        %>
    </body>

</html>