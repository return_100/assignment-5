package net.therap.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author al.imran
 * @since 13/04/2021
 */
@Entity
@Table(name = "course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqCourse", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCourse")
    private int id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "enrolledCourses")
    private Set<Trainee> traineeSet;

    @Transient
    private int traineeSetSize;

    public Course() {
        this.traineeSet = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Trainee> getTraineeSet() {
        return traineeSet;
    }

    public void setTraineeSet(Set<Trainee> traineeSet) {
        this.traineeSet = traineeSet;
    }

    public int getTraineeSetSize() {
        return traineeSet.size();
    }

    public void setTraineeSetSize(int traineeSetSize) {
        this.traineeSetSize = traineeSetSize;
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.nonNull(o) && (o instanceof Course)) {
            return (id == ((Course) o).getId() && name.equals(((Course) o).getName()));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return (id + " " + name);
    }
}
