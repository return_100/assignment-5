package net.therap.service;

import net.therap.dao.AdminDao;
import net.therap.dao.CourseDao;
import net.therap.dao.Dao;
import net.therap.dao.TraineeDao;
import net.therap.model.Admin;
import net.therap.model.Course;
import net.therap.model.Trainee;

import java.util.List;
import java.util.Objects;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class AdminService {

    private final Dao<Admin> adminDao;
    private final Dao<Course> courseDao;

    public AdminService() {
        this.adminDao = new AdminDao();
        this.courseDao = new CourseDao();
    }

    public Admin login(String email, String password) {
        Admin admin = new Admin();
        admin.setEmail(email);
        admin.setPassword(password);
        return adminDao.find(admin);
    }

    public Admin getAdmin(int id) {
        return adminDao.findById(id);
    }

    public List<Trainee> getAllTrainee() {
        Dao<Trainee> traineeDao = new TraineeDao();
        return traineeDao.findAll();
    }

    public List<Course> getAllCourse() {
        return courseDao.findAll();
    }

    public boolean addCourse(String coursename) {
        Course course = new Course();
        course.setName(coursename);
        return courseDao.save(course);
    }

    public Course getCourse(int id) {
        return courseDao.findById(id);
    }

    public void addToken(Admin admin, String token) {
        admin.setSessionToken(token);
        adminDao.update(admin);
    }

    public boolean isCourseExist(String coursename) {
        Course course = new Course();
        course.setName(coursename);
        return Objects.nonNull(courseDao.find(course));
    }

    public void deleteCourse(String coursename) {
        Course course = new Course();
        course.setName(coursename);
        course = courseDao.find(course);
        courseDao.delete(course);
    }
}
