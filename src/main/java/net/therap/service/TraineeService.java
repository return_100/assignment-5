package net.therap.service;

import net.therap.dao.CourseDao;
import net.therap.dao.Dao;
import net.therap.dao.TraineeDao;
import net.therap.model.Course;
import net.therap.model.Trainee;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 15/04/2021
 */
public class TraineeService {

    private static final String EMAIL_REGEX = "[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+" +
            "\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*";

    private final TraineeDao traineeDao;
    private final Dao<Course> courseDao;

    public TraineeService() {
        this.traineeDao = new TraineeDao();
        this.courseDao = new CourseDao();
    }

    public boolean register(String name, String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee.setName(name);
        trainee.setPassword(password);

        if (Pattern.matches(EMAIL_REGEX, email)) {
            return traineeDao.save(trainee);
        }

        return false;
    }

    public Trainee login(String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee = traineeDao.find(trainee);

        if (Objects.nonNull(trainee)) {
            if (trainee.getPassword().equals(password)) {
                return trainee;
            } else {
                return null;
            }
        } else{
            return null;
        }
    }

    public Trainee getTrainee(int id) {
        return traineeDao.findById(id);
    }

    public boolean isTraineeRegisterWithEmail(String email) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        return Objects.nonNull(traineeDao.find(trainee));
    }

    public List<Course> getAllCourses() {
        return courseDao.findAll();
    }

    public void addToken(Trainee trainee, String token) {
        trainee.setSessionToken(token);
        traineeDao.update(trainee);
    }

    public boolean isCourseExist(String coursename) {
        Course course = new Course();
        course.setName(coursename);
        return Objects.nonNull(courseDao.find(course));
    }

    public void enrolledInACourse(int traineeId, String coursename) {
        Course course = new Course();
        course.setName(coursename);
        course = courseDao.find(course);

        Trainee trainee = traineeDao.findById(traineeId);
        trainee.getEnrolledCourses().add(course);
        course.getTraineeSet().add(trainee);

        traineeDao.update(trainee);
    }

    public void unenrolledFromACourse(int traineeId, String coursename) {
        Course course = new Course();
        course.setName(coursename);
        course = courseDao.find(course);

        Trainee trainee = traineeDao.findById(traineeId);
        trainee.getEnrolledCourses().remove(course);
        course.getTraineeSet().remove(trainee);

        traineeDao.update(trainee);
        courseDao.update(course);
    }
}
