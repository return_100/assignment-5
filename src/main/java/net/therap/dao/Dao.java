package net.therap.dao;

import net.therap.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
public interface Dao<T> {

    default T find(T t) {
        return null;
    }

    default List<T> findAll() {
        return null;
    }

    default T findById(int id) {
        return null;
    }

    default boolean save(T t) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(t);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }

        return false;
    }

    default boolean update(T t) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            entityManager.getTransaction().begin();
            entityManager.merge(t);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }

        return false;
    }

    default boolean delete(T t) {
        return false;
    }
}
