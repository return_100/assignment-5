package net.therap.dao;

import net.therap.model.Trainee;
import net.therap.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
public class TraineeDao implements Dao<Trainee> {

    private static final String JPQL_FIND = "FROM Trainee " +
            "WHERE email = :email";

    private static final String JPQL_FIND_ALL = "FROM Trainee";

    @Override
    public Trainee find(Trainee trainee) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.createQuery(JPQL_FIND, Trainee.class)
                    .setParameter("email", trainee.getEmail())
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Trainee> findAll() {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.createQuery(JPQL_FIND_ALL, Trainee.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    @Override
    public Trainee findById(int id) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.find(Trainee.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
