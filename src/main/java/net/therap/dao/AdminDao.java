package net.therap.dao;

import net.therap.model.Admin;
import net.therap.util.EntityManagerUtil;

import javax.persistence.EntityManager;

/**
 * @author al.imran
 * @since 14/04/2021
 */
public class AdminDao implements Dao<Admin> {

    private static final String JPQL_FIND = "FROM Admin " +
            "WHERE email = :email and password = :password";

    @Override
    public Admin find(Admin admin) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.createQuery(JPQL_FIND, Admin.class)
                    .setParameter("email", admin.getEmail())
                    .setParameter("password", admin.getPassword())
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Admin findById(int id) {
        EntityManager entityManager = EntityManagerUtil.getEntityManager();

        try {
            return entityManager.find(Admin.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
