package net.therap.validator;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 05/05/2021
 */
public class NameValidator implements Validator<String> {

    private static final int MIN_NAME_LENGTH = 1;
    private static final int MAX_NAME_LENGTH = 6;
    private static final String NAME_REGEX = "[a-zA-Z]+[a-zA-Z0-9]*";

    @Override
    public boolean check(String s) {
        return Objects.nonNull(s) &&
                s.length() >= MIN_NAME_LENGTH &&
                s.length() <= MAX_NAME_LENGTH &&
                Pattern.matches(NAME_REGEX, s);
    }
}
