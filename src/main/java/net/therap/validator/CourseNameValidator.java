package net.therap.validator;

import java.util.Objects;

/**
 * @author al.imran
 * @since 05/05/2021
 */
public class CourseNameValidator implements Validator<String> {

    private static final int MIN_COURSE_NAME_LENGTH = 1;
    private static final int MAX_COURSE_NAME_LENGTH = 6;

    @Override
    public boolean check(String s) {
        return Objects.nonNull(s) &&
                s.length() <= MAX_COURSE_NAME_LENGTH &&
                s.length() >= MIN_COURSE_NAME_LENGTH;
    }
}
