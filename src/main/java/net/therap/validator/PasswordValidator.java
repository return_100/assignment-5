package net.therap.validator;

import java.util.Objects;

/**
 * @author al.imran
 * @since 05/05/2021
 */
public class PasswordValidator implements Validator<String> {

    private static final int MIN_PASS_LENGTH = 4;
    private static final int MAX_PASS_LENGTH = 30;

    @Override
    public boolean check(String s) {
        return Objects.nonNull(s) &&
                s.length() >= MIN_PASS_LENGTH &&
                s.length() <= MAX_PASS_LENGTH;
    }
}
