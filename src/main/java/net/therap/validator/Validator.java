package net.therap.validator;

/**
 * @author al.imran
 * @since 05/05/2021
 */
public interface Validator<T> {

    boolean check(T t);
}
