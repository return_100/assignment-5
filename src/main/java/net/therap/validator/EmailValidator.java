package net.therap.validator;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 05/05/2021
 */
public class EmailValidator implements Validator<String> {

    private static final String EMAIL_REGEX = "[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+" +
            "\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*";
    private static final int MAX_EMAIL_LENGTH = 30;

    @Override
    public boolean check(String s) {
        return Objects.nonNull(s) &&
                Pattern.matches(EMAIL_REGEX, s) &&
                s.length() <= MAX_EMAIL_LENGTH;
    }
}
