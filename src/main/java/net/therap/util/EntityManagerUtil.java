package net.therap.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author al.imran
 * @since 13/04/2021
 */
public class EntityManagerUtil {

    private static EntityManager entityManager;

    public static void init() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("training");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    public static void closeEntityManager() {
        entityManager.close();
    }

    public static boolean isEntityManagerOpen() {
        return entityManager.isOpen();
    }
}
