package net.therap.servlet.trainee;

import net.therap.service.TraineeService;
import net.therap.validator.CourseNameValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 26/04/2021
 */
@WebServlet("/traineeaddcourse")
public class CourseEnrolledServlet extends HttpServlet {

    private TraineeService traineeService;
    private CourseNameValidator courseNameValidator;

    @Override
    public void init() throws ServletException {
        this.traineeService = new TraineeService();
        this.courseNameValidator = new CourseNameValidator();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = (Integer) req.getSession().getAttribute("id");
        String courseName = req.getParameter("courseName");

        if (courseNameValidator.check(courseName)) {
            if (traineeService.isCourseExist(courseName)) {
                traineeService.enrolledInACourse(id, courseName);
                resp.sendRedirect("enrolledcourse");
            } else {
                req.setAttribute("message", "Course Doesn't Exist");
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/trainee/addCourse.jsp");
                requestDispatcher.forward(req, resp);
            }
        } else {
            req.setAttribute("message", "Invalid Course Format or Length");
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/trainee/addCourse.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/trainee/addCourse.jsp");
        requestDispatcher.forward(req, resp);
    }
}
