package net.therap.servlet.trainee;

import net.therap.model.Trainee;
import net.therap.service.TraineeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 26/04/2021
 */
@WebServlet("/enrolledcourse")
public class CourseShowEnrolledServlet extends HttpServlet {

    private TraineeService traineeService;

    @Override
    public void init() throws ServletException {
        this.traineeService = new TraineeService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = (Integer) req.getSession().getAttribute("id");
        Trainee trainee = traineeService.getTrainee(id);

        req.setAttribute("enrolledcourse", trainee.getEnrolledCourses());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/trainee/enrolledCourse.jsp");
        requestDispatcher.forward(req, resp);
    }
}
