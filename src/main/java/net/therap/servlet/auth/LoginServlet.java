package net.therap.servlet.auth;

import net.therap.model.Admin;
import net.therap.model.Trainee;
import net.therap.service.AdminService;
import net.therap.service.TraineeService;
import net.therap.util.EntityManagerUtil;
import net.therap.validator.EmailValidator;
import net.therap.validator.PasswordValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

/**
 * @author al.imran
 * @since 21/04/2021
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String ROLE_ADMIN = "Admin";
    private static final String ROLE_TRAINEE = "Trainee";

    private AdminService adminService;
    private TraineeService traineeService;

    private EmailValidator emailValidator;
    private PasswordValidator passwordValidator;

    @Override
    public void init() throws ServletException {
        this.adminService = new AdminService();
        this.traineeService = new TraineeService();
        this.emailValidator = new EmailValidator();
        this.passwordValidator = new PasswordValidator();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String email = req.getParameter(EMAIL);
        String password = req.getParameter(PASSWORD);
        String role = req.getParameter("role");

        if (ROLE_ADMIN.equals(role)) {
            if (emailValidator.check(email) && passwordValidator.check(password)) {
                EntityManagerUtil.init();

                Admin admin = adminService.login(email, password);

                if (Objects.nonNull(admin)) {
                    String token = UUID.randomUUID().toString().toUpperCase();
                    adminService.addToken(admin, token);

                    HttpSession httpSession = req.getSession();
                    httpSession.setAttribute("token", token);
                    httpSession.setAttribute("id", admin.getId());
                    httpSession.setAttribute("role", ROLE_ADMIN);

                    resp.sendRedirect("allcourse");
                } else {
                    resp.sendRedirect("/");
                }
            } else {
                resp.sendRedirect("/");
            }
        } else if (ROLE_TRAINEE.equals(role)) {
            if (emailValidator.check(email) && passwordValidator.check(password)) {
                EntityManagerUtil.init();

                Trainee trainee = traineeService.login(email, password);

                if (Objects.nonNull(trainee)) {
                    String token = UUID.randomUUID().toString().toUpperCase();
                    traineeService.addToken(trainee, token);

                    HttpSession httpSession = req.getSession();
                    httpSession.setAttribute("token", token);
                    httpSession.setAttribute("id", trainee.getId());
                    httpSession.setAttribute("role", ROLE_TRAINEE);

                    resp.sendRedirect("enrolledcourse");
                } else {
                    resp.sendRedirect("/");
                }
            } else {
                resp.sendRedirect("/");
            }
        } else {
            resp.sendRedirect("/");
        }
    }
}
