package net.therap.servlet.filter;

import net.therap.model.Admin;
import net.therap.model.Trainee;
import net.therap.service.AdminService;
import net.therap.service.TraineeService;
import net.therap.util.EntityManagerUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author al.imran
 * @since 24/04/2021
 */
@WebFilter(value = {"/*"}, dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class TokenVerifyServlet implements Filter {

    private static final String ROLE_ADMIN = "Admin";
    private static final String ROLE_TRAINEE = "Trainee";

    private AdminService adminService;
    private TraineeService traineeService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.adminService = new AdminService();
        this.traineeService = new TraineeService();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

        boolean isInitialLandingPath = req.getRequestURI().equals("/");
        boolean isLoginPage = req.getRequestURI().endsWith("login.jsp");
        boolean isLoginValidationPath = req.getRequestURI().startsWith("/login");
        boolean isPublicPath = req.getRequestURI().startsWith("/public");
        boolean isFavicon = req.getRequestURI().startsWith("/favicon");

        if (isInitialLandingPath || isLoginPage || isLoginValidationPath || isPublicPath || isFavicon) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            if (!EntityManagerUtil.isEntityManagerOpen()) {
                EntityManagerUtil.init();
            }

            HttpSession httpSession = req.getSession();

            int id = (Integer) httpSession.getAttribute("id");
            String token = (String) httpSession.getAttribute("token");
            String role = (String) httpSession.getAttribute("role");

            if (ROLE_ADMIN.equals(role)) {
                Admin admin = adminService.getAdmin(id);

                if (admin.getSessionToken().equals(token)) {
                    try {
                        filterChain.doFilter(servletRequest, servletResponse);
                    } catch (ServletException e) {
                        resp.sendRedirect("/");
                    }
                } else {
                    resp.sendRedirect("/");
                }
            } else if (ROLE_TRAINEE.equals(role)) {
                Trainee trainee = traineeService.getTrainee(id);

                if (trainee.getSessionToken().equals(token)) {
                    try {
                        filterChain.doFilter(servletRequest, servletResponse);
                    } catch (ServletException e) {
                        resp.sendRedirect("/");
                    }
                } else {
                    resp.sendRedirect("/");
                }
            } else {
                resp.sendRedirect("/");
            }
        }
    }

    @Override
    public void destroy() {

    }
}
