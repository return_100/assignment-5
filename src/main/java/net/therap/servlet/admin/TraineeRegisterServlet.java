package net.therap.servlet.admin;

import net.therap.service.TraineeService;
import net.therap.validator.EmailValidator;
import net.therap.validator.NameValidator;
import net.therap.validator.PasswordValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 27/04/2021
 */
@WebServlet("/register")
public class TraineeRegisterServlet extends HttpServlet {

    private TraineeService traineeService;
    private EmailValidator emailValidator;
    private NameValidator nameValidator;
    private PasswordValidator passwordValidator;

    @Override
    public void init() throws ServletException {
        this.traineeService = new TraineeService();
        this.emailValidator = new EmailValidator();
        this.nameValidator = new NameValidator();
        this.passwordValidator = new PasswordValidator();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String repassword = req.getParameter("repassword");

        boolean isBadFormat = false;

        if (!nameValidator.check(name)) {
            req.setAttribute("message", "Invalid Name Format or Length");
            isBadFormat = true;
        } else if (!emailValidator.check(email)) {
            req.setAttribute("message", "Invalid Email Format or Length");
            isBadFormat = true;
        } else if (!passwordValidator.check(password)) {
            req.setAttribute("message", "Invalid Password Format or Length");
            isBadFormat = true;
        } else if (!password.equals(repassword)) {
            req.setAttribute("message", "Password Mismatch");
            isBadFormat = true;
        } else {
            if (traineeService.isTraineeRegisterWithEmail(email)) {
                req.setAttribute("message", "Email already Registered");
                isBadFormat = true;
            } else {
                traineeService.register(name, email, password);
                resp.sendRedirect("traineelist");
            }
        }

        if (isBadFormat) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/traineeRegistration.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/traineeRegistration.jsp");
        requestDispatcher.forward(req, resp);
    }
}
