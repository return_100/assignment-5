package net.therap.servlet.admin;

import net.therap.service.AdminService;
import net.therap.validator.CourseNameValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 25/04/2021
 */
@WebServlet("/deletecourse")
public class CourseDeleteServlet extends HttpServlet {

    private AdminService adminService;
    private CourseNameValidator courseNameValidator;

    @Override
    public void init() throws ServletException {
        this.adminService = new AdminService();
        this.courseNameValidator = new CourseNameValidator();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String courseName = req.getParameter("courseName");

        if (courseNameValidator.check(courseName)) {
            if (adminService.isCourseExist(courseName)) {
                adminService.deleteCourse(courseName);
                resp.sendRedirect("allcourse");
            } else {
                req.setAttribute("message", "Course Doesn't Exist");
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/deleteCourse.jsp");
                requestDispatcher.forward(req, resp);
            }
        } else {
            req.setAttribute("message", "Invalid Course Format or Length");
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/deleteCourse.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/deleteCourse.jsp");
        requestDispatcher.forward(req, resp);
    }
}
