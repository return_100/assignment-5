package net.therap.servlet.admin;

import net.therap.service.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 23/04/2021
 */
@WebServlet("/allcourse")
public class CourseShowAllServlet extends HttpServlet {

    private AdminService adminService;

    @Override
    public void init() throws ServletException {
        this.adminService = new AdminService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setAttribute("allcourse", adminService.getAllCourse());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/allCourse.jsp");
        requestDispatcher.forward(req, resp);
    }
}
