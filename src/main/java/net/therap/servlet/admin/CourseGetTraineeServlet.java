package net.therap.servlet.admin;

import net.therap.model.Course;
import net.therap.service.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 26/04/2021
 */
@WebServlet("/gettraineelist")
public class CourseGetTraineeServlet extends HttpServlet {

    private AdminService adminService;

    @Override
    public void init() throws ServletException {
        this.adminService = new AdminService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = (Integer) req.getSession().getAttribute("courseId");
        Course course = adminService.getCourse(id);

        req.setAttribute("trainees", course.getTraineeSet());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/traineeList.jsp");
        requestDispatcher.forward(req, resp);
    }
}
