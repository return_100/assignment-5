package net.therap.servlet.admin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 29/04/2021
 */
@WebServlet("/traineelistredirect")
public class TraineeRedirectServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getSession().setAttribute("courseId", Integer.parseInt(req.getParameter("id")));
        resp.sendRedirect("gettraineelist");
    }
}
