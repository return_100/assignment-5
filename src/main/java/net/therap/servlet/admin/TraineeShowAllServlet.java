package net.therap.servlet.admin;

import net.therap.service.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author al.imran
 * @since 28/04/2021
 */
@WebServlet("/traineelist")
public class TraineeShowAllServlet extends HttpServlet {

    private AdminService adminService;

    @Override
    public void init() throws ServletException {
        this.adminService = new AdminService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("traineelist", adminService.getAllTrainee());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/jsp/admin/allTraineeList.jsp");
        requestDispatcher.forward(req, resp);
    }
}
